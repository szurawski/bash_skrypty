#!/bin/bash
HELP="\n  
-------------------------------------------------------------------------\n
|-------- toFloor jest skryptem zamieniającym w nazwach plików ---------|\n
|\t        katalogu,  w którym się znajduje ( został uruchomiony )\t\t      | \n
|\t\t  znak 'spacji' na znak podkreślenia '_' . \t\t |\n
| \t**WIZUALIZACJA: NAZWA PLIKU.TXT --> NAZWA_PLIKU.TXT**\t\t |\n
-------------------------------------------------------------------------"
				#powyżej zmienna HELP z przypisaniem zawartości
case $1		 		#sprawdzenie pierwszego parametru na wejściu
in
--help)
	echo -e $HELP		 #dla parametru wejścia "--help" wyświetl zawartość zmiennej HELP
exit
       ;;
-h)
	echo -e $HELP		 #dla parametru wejścia "-h" wyświetl zawartość zmiennej HELP


exit
;;
*)				#dla żadnego, bądz innych niż podane wyżej parametry wykonaj:

for I in *			#
do 
/bin/mv "$I" "`echo $I | tr ' ' '_'`";
done;
/bin/ls
exit
;;

esac
