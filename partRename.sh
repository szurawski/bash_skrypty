#!/bin/bash
HELP="\n  
----------------------------------------------------------------------------\n\n
\t\t\t\t***partRename***\n\n
partRename jest skryptem zamieniającym wskazaną część nazwy plików katalogu,\n
w którym się znajduje.\n
Skrypt przyjmuje 2 parametry, które należy podać przy uruchomieniu skryptu.\n\n
\t\t\t\t--- WZÓR:---\n\n
\t\t$ bash partRename.sh PARAMETR1 PARAMETR2\n\n
- PARAMETR1 określa występującą frazę w nazwie pliku,\n
\t którą chcemy zmienić.\n
- PARAMETR2 określa frazę docelową,\n
\t którą chcemy uzyskać po zmianie w nazwie pliku.\n\n 
\t\t\t\t--- PRZYKŁAD:---\n\n
\tW katalogu posiadamy pliki o nazie moja_dupa.txt, dupa_mala.txt\n
Potrzebujemy zamienić człon 'dupa' na np 'szafa'\n\n
\tCo robimy?? Prościzna:\n\n
\t\t\t partRename.sh dupa szafa\n\n
I tak otrzymujemy  pliki o nazwach: moja_szafa.txt oraz szafa_mala.txt\n
----------------------------------------------------------------------------"
                                #powyżej zmienna HELP z przypisaniem zawartości
case $1                         #sprawdzenie pierwszego parametru na wejściu
in
--help)
        echo -e $HELP            #dla parametru wejścia "--help" wyświetl zawartość zmiennej HELP
exit
       ;;
-h)
        echo -e $HELP            #dla parametru wejścia "-h" wyświetl zawartość zmiennej HELP

exit
;;
*)				#dla innych przypadków wykona co niżej
PLIKI="$(ls | grep "$1")"       #zapisuje do zmiennej PLIKI znalezione pliki, które posiadają w nazwach ciąg znaków podany w parametrze1
for PLIK in $PLIKI;		
do
/bin/mv "$PLIK" "`echo $PLIK | sed -e 's|'$1'|'$2'|g;'`"; # sed podmienia frazy podane w parametrach (1 na 2) i podstawia zmienione nazwy do wyświetlenia i nadpisania
done;
exit
;;
esac

